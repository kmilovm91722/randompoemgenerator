package com.macfarnet.poemgenerator;
import java.io.File;

public class RandomPoemGenerator {
	public static void main(String[] args) {
		if (args.length > 0) {
			File grammar = new File(args[0]);
			System.out.println(new PoemParser(grammar).generateRandomPoem("<POEM>"));
		} else {
			System.err.println("Usage: java -jar RandomPoemGenerator <GrammarPath>");
			System.exit(0);
		}
	}
}
