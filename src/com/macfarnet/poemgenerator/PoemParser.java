package com.macfarnet.poemgenerator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

public class PoemParser {

	public static final String LINEBREAK = "$LINEBREAK";
	public static final String END = "$END";
	private HashMap<String, String> grammar;
	private String poem;

	public PoemParser(File grammar) {
		this.grammar = getGrammarFromFile(grammar);
	}

	public String generateRandomPoem(String mainRule) {
		this.poem = "";
		parseRule(mainRule);
		return this.poem;
	}

	/**
	 * Returns a valid random line following recursively the rule definitions on
	 * the given grammar and populates a private global variable with the stack
	 * result
	 * 
	 * @param rule
	 *            a String with a ruleDefinition
	 * @return A String with a valid line defined by the given grammar
	 */
	private String parseRule(String rule) {
		// Split the rule in segments by " ", so, we can parse each
		// segment and generate a valid string
		String[] segments = rule.split(" ");
		String poem = "";
		for (String segment : segments) {
			// If the segment contains "|", we select a random option
			// and then parse it again
			if (segment.contains("|")) {
				String[] options = segment.split("\\|");
				String option = options[new Random().nextInt(options.length)];
				poem += parseRule(option);
			} else {
				if (segment.contains("<")) {
					// If the segment contains <, then it's a token
					// and we need to parse it again
					String ruleName = segment.substring(
							segment.indexOf('<') + 1, segment.indexOf('>'));
					String secondRuleDefinition = grammar.get(ruleName);
					parseRule(secondRuleDefinition);
				} else {
					switch (segment) {
					case LINEBREAK:
						poem += "\n";
						break;
					case END:
						return poem;
					default:
						poem += " " + segment;
						break;
					}
				}
			}
		}
		this.poem += poem;
		return poem;
	}

	/**
	 * Returns a HashMap object representing the definition of a grammar with
	 * the format: <ruleName, ruleDefinition> from the given file
	 * 
	 * @param file
	 *            a File object containing the grammar definition
	 * @return A HashMap with the grammar definition
	 */
	private HashMap<String, String> getGrammarFromFile(File file) {
		BufferedReader br = null;
		try {
			HashMap<String, String> rules = new HashMap<String, String>();
			String currentLine;
			br = new BufferedReader(new FileReader(file));
			while ((currentLine = br.readLine()) != null) {
				// Take the string before the colon as the ruleName
				String ruleName = currentLine.substring(0,
						currentLine.indexOf(':'));
				// Take the string after the colon as the ruleDefinition
				String ruleDefinition = currentLine.substring(
						currentLine.indexOf(':') + 2, currentLine.length());
				rules.put(ruleName, ruleDefinition);
			}
			return rules;

		} catch (IOException e) {
			System.out.println("There was a problem reading the given file");
			System.exit(0);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}
}
