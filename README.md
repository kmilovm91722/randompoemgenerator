# README #

This is my solution for the MacFarnet technical test

### Details ###

* This is a program that generates a random poem reading the following grammar from a file:

```
#!ruby

POEM: <LINE> <LINE> <LINE> <LINE> <LINE>
LINE: <NOUN>|<PREPOSITION>|<PRONOUN> $LINEBREAK
ADJECTIVE: black|white|dark|light|bright|murky|muddy|clear <NOUN>|<ADJECTIVE>|$END
NOUN: heart|sun|moon|thunder|fire|time|wind|sea|river|flavor|wave|willow|rain|tree|flowe r|field|meadow|pasture|harvest|water|father|mother|brother|sister <VERB>|<PREPOSITION>|$END
PRONOUN: my|your|his|her <NOUN>|<ADJECTIVE>
VERB: runs|walks|stands|climbs|crawls|flows|flies|transcends|ascends|descends|sinks <PREPOSITION>|<PRONOUN>|$END
PREPOSITION: above|across|against|along|among|around|before|behind|beneath|beside|between|beyon d|during|inside|onto|outside|under|underneath|upon|with|without|through <NOUN>|<PRONOUN>|<ADJECTIVE>
```

* Version 1.0

### How do I get set up? ###

1. Generate a jar or just download it compiled from [here](https://drive.google.com/file/d/0BzeOjH8SC7bpRjdleWZLWVlrV1k/edit?usp=sharing)
2. Run it with:

```
#!bash

$ java -jar RandomPoemGenerator /path/to/the/poem/grammar
```

### Class Diagram ###

![classDiagram.png](https://bitbucket.org/repo/KGpzax/images/193486373-classDiagram.png)

### Questions? ###

Send me an email: danielcamilovergara@gmail.com